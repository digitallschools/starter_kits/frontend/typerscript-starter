import { Branch } from './branch';

export class Bank {
  name: string;
  headquarters: string;
  branches: Branch[] = [];

  constructor(bankName: string, headquarters: string) {
    this.name = bankName;
    this.headquarters = headquarters;
  }

  addBranch(branch: Branch) {
    this.branches.push(branch);
  }
}
