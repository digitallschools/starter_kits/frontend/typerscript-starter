export class Branch {
  name: string;
  location: string;

  constructor(branchName: string, location: string) {
    this.name = branchName;
    this.location = location;
  }
}
